console.log("Hello, World!");

// [SECTION] Function Declaration

/*
    The statements and instructions inside a function is not immediately executed when the function is defined.
    They are run/executed when a function is invoked.
    To invoke a declared function, add the name of the function and a parenthesis.
*/

// Hoisting - javascript behavior of certain variables and functions to run even before their declaration

printName();

function printName() {
	console.log("My name is John.");
}

printName();

// Function Expression

let variableFunction = function myGreetings() {
	console.log("Hi");
}


// In function expression i.e. whenever a function is declared inside a variable, you use the variable name instead of the function name. Using the function name will result to an error.

// As a result, hoisting is not possible with Function Expressions

// Re-Assigning Functions

variableFunction = function myGreetings() {
	console.log("Hello!");
}

variableFunction();

// Function Scoping

// Scope is the accessibility of variables

/*
	1. Local/Block Scope
	2. Global Scope
	3. Function Scope
*/

	// Local Variable

		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

	// Global Variable
		
		let globalVar = "Mr. Worldwide"

		console.log(globalVar);
		{
			console.log(globalVar);
		}

	// Function Scope

	function showNames() {
		// Function scoped variables
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionConst);
		console.log(functionLet);		
	}

	showNames();

	// console.log(functionConst); - error
	// console.log(functionLet); - error

// alert() and prompt()

	// alert() - allows us to show a small window at the top of our browser page to show information to our users.

	//`it will have the page wait until the user clicks OK.` 

		// alert("Hello, User!");

		// function showSampleAlert() {
		// 	alert("Hello User!")
		// }

		// showSampleAlert();
	
	// prompt() - allows us to show a small window at the top of the browser to gather user input.

	// Much like alert(), it will have the page wait until the user completes or enters their input.

		let samplePrompt = prompt("Enter your Name: ");
		console.log("Hello, " + samplePrompt + "!");

		function printWelcomeMessages() {
			let firstName = prompt("Enter your First Name:");
			let lastName = prompt("Enter your Last Name:");

			console.log("Hello, " + firstName + " " + lastName + ".");
			console.log("Welcome to Mobile Legends!")
		}

		printWelcomeMessages();